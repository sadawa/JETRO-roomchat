import tkinter
import socket
from tkinter import *
import threading


def receive():
    while True:
        try:
            msg = s.recv(1024).decode('utf8')
            msg_list.insert(tkinter.END,msg)
        except:
          print("ERRUR Message non reçu")


def send():
    msg = my_msg.get()
    my_msg.set('')
    s.send(bytes(msg,'utf8'))
    if msg == "#quit":
        s.close()
        window.close()


def on_closing():
    my_msg.set("#quit")
    send()

window = Tk()
window.title('Chat Room JTERO')
window.configure(bg="black")


message_frame = Frame(window,height=100,width=100,bg='Red')

message_frame.pack()

my_msg=StringVar()
my_msg.set('')

scroll_bar= Scrollbar(message_frame)
msg_list = Listbox(message_frame,height=15,width=100,bg='#2f3237',yscrollcommand=scroll_bar.set)
scroll_bar.pack(side=RIGHT,fill=Y)
msg_list.pack(side=LEFT,fill=BOTH)
msg_list.pack()

label = Label(window,text="Entre le message",fg="#FF7F11",font="Aeria",bg="#ffffff")
label.pack()

entre_field = Entry(window,textvariable=my_msg,fg="red",width=50)
entre_field.pack()

send_Button = Button(window,text="Envoyer",font="Aerial",fg="white",command=send)
send_Button.pack()

quit_Button = Button(window,text="Quitter",font="Aerial",fg="white",command=on_closing)
quit_Button.pack()

Host= "localhost"
Port= 8080

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect((Host,Port))

receive_Thread = threading.Thread(target=receive)
receive_Thread.start()
mainloop()