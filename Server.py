import socket
import threading
host = "localhost"
port = 8080

clients = {}
addresses = {}

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((host, port))
def handle_clients(conn,address):
    name = conn.recv(1024).decode()
    welcome = "Bonjour" + " " + name + ". Tu peux quitte le chat quand tu veux ta juste a clique sur quitter"
    conn.send(bytes(welcome, 'utf8'))
    msg = name + " Récemment vous avez rejoint le chat room "
    broadcast(bytes(msg, 'utf8'))
    clients[conn] = name

    while True:
        msg = conn.recv(1024)
        if msg != bytes("#quit", "utf8"):
            broadcast(msg, name+":")
        else:
            conn.send(bytes("#quit", 'utf8'))
            conn.close()
            del clients[conn]
            broadcast(bytes(name + 'A quitter le chat room',"utf8"))



def accept_client_connections():
    while True:
        client_conn, client_address = sock.accept()
        print(client_address, "I Has Connected")
        client_conn.send('Bienvenue dans le chat room , bg merci de rentre psuedo pour continuer'.encode("utf8"))
        addresses[client_conn] = client_address
        threading.Thread(target=handle_clients, args=(client_conn, client_address)).start()

def broadcast(msg, prefix = ""):
    for x in clients:
        x.send(bytes(prefix, 'utf8')+msg)

if __name__ == '__main__':
    sock.listen(2)
    print("The server is running and is listening to client to requests")

    t1 = threading.Thread(target=accept_client_connections)
    t1.start()
    t1.join()

